<?php  
session_start();

// connect to database
$db = mysqli_connect('localhost', 'root', '', 'employmanangement');

// variable declaration
$username = "";
$email    = "";
$errors   = array(); 
global $primary_key;
// call the register() function if register_btn is clicked
if (isset($_POST['add_employ_btn'])) {
	add_employ();
}

// REGISTER USER
function add_employ(){
	// call these variables with the global keyword to make them available in function
	global $db, $errors, $username, $email;

	// receive all input values from the form. Call the e() function
    // defined below to escape form values
	$name   =  e($_POST['name']);
	$employID    =  e($_POST['employID']);
	$phoneNo	 = e($_POST['phoneNo']);
	$email       =  e($_POST['email']);
	$user_type   =  e($_POST['user_type']);
	$password    =  e($_POST['password']);
	// form validation: ensure that the form is correctly filled
	if (empty($firstname)) { 
		array_push($errors, "Name is required"); 
	}
	if (empty($employID)) { 
		array_push($errors, "Employ Id Id is required"); 
	}
	if (empty($phoneNo)) {
		array_push($errors,"Phone NO is required");
	}
	if (empty($email)) { 
		array_push($errors, "Email is required"); 
	}
	if (empty($user_type)) { 
		array_push($errors, "User Type is required"); 
	}
	if (empty($password_1)) { 
		array_push($errors, "Password is required"); 
	}
	// register user if there are no errors in the form
	if (count($errors) == 0) {
	
			$query = "INSERT INTO empoly_details (primary_key,name,employ_id,email,phone_no,user_type,password) 
					 VALUES('','$name,'$employId','$email','$phoneNo',$user_type, $password')";
			mysqli_query($db, $query);
			$_SESSION['success']  = "New user successfully created!!";
			header('location: add_empoloy.php');
		}
	}


// return user array from their id
function getUserById($primary_key){
	global $db;
	$query = "SELECT * FROM employ_details WHERE primary_key= $primary_key";
	$result = mysqli_query($db, $query);

	$user = mysqli_fetch_assoc($result);
	return $user;
}

// escape string
function e($val){
	global $db;
	return mysqli_real_escape_string($db, trim($val));
}

function display_error() {
	global $errors;

	if (count($errors) > 0){
		echo '<div class="error">';
			foreach ($errors as $error){
				echo $error .'<br>';
			}
		echo '</div>';
	}
}	
function isLoggedIn()
{
	if (isset($_SESSION['user'])) {
		return true;
	}else{
		return false;
	}
}
// log user out if logout button clicked
if (isset($_GET['logout'])) {
	session_destroy();
	unset($_SESSION['user']);
	header("location: login.php");
}
// call the login() function if register_btn is clicked
if (isset($_POST['login_btn'])) {
	login();
}

// LOGIN USER
function login(){
	global $db, $username, $errors;

	// grap form values
	$username = e($_POST['username']);
	$password = e($_POST['password']);

	// make sure form is filled properly
	if (empty($username)) {
		array_push($errors, "Username is required");
	}
	if (empty($password)) {
		array_push($errors, "Password is required");
	}

	// attempt login if no errors on form
	if (count($errors) == 0) {
		$query = "SELECT * FROM empoly_details WHERE employ_id='$username' AND password='$password' LIMIT 1";
		$results = mysqli_query($db, $query);
		if (mysqli_num_rows($results) == 1) { // user found
			// check if user is admin or user
			$logged_in_user = mysqli_fetch_assoc($results);
			if ($logged_in_user['user_type'] == 'admin') {

				$_SESSION['user'] = $logged_in_user;
				$_SESSION['primary_key'] = $logged_in_user['primary_key'];  	
				$_SESSION['success']  = "You are now logged in as admin";
				header('location: manager_home.php');		  
			}else{
				$_SESSION['user'] = $logged_in_user;
				$_SESSION['primary_key'] = $logged_in_user['primary_key']; 
				$_SESSION['name'] = $logged_in_user['name'];
				$_SESSION['success']  = "You are now logged in as user";
				header('location: trainee_home_emp.php');
			}
		}else {
			array_push($errors, "Wrong username/password combination");
		}
	}
}
function isAdmin()
{
	if (isset($_SESSION['user']) && $_SESSION['user']['user_type'] == 'admin' ) {
		return true;
	}else{
		return false;
	}
}


if (isset($_POST['add_task_btn'])) {
	add_task();
}

// Add Question Set
function add_task(){
	// call these variables with the global keyword to make them available in function
	global $db, $errors, $username, $email;

	// receive all input values from the form. Call the e() function
    // defined below to escape form values
	$task_id      =  e($_POST['task_id']);
	$task_name    =  e($_POST['task_name']);
	$task_details =  e($_POST['task_details']);
	$duration     =  e($_POST['duration']);

	// form validation: ensure that the form is correctly filled
	if (empty($task_id)) { 
		array_push($errors, "Task Id is required"); 
	}
	if (empty($task_name)) { 
		array_push($errors, "Task Name is required"); 
	}
	if (empty($task_details)) { 
		array_push($errors, "Task Details is required"); 
	}
	if (empty($efficiency)) { 
		array_push($errors, "Duration is required"); 
	}
		
	// register user if there are no errors in the form
	if (count($errors) == 0) {
			$query = "INSERT INTO `task_details`(`id`, `task_id`, `taks_name`, `task_detail`, `duration`) VALUES ('','$task_id','$task_name','$task_details','$duration') ";
			mysqli_query($db, $query);
			$_SESSION['success']  = "New user successfully created!!";
			
		}
	}
	
if (isset($_POST['delete_btn'])) {
	delete_employ();
}

// Delete USER


if (isset($_POST['attendace_btn'])) {
	attendance();
}

// Delete Question
function attendace(){
	// call these variables with the global keyword to make them available in function
	global $db, $errors, $username, $email;

	// receive all input values from the form. Call the e() function
    // defined below to escape form values
	$trainee_id   =  e($_POST['trainee_id']);

	// form validation: ensure that the form is correctly filled
	if (empty($question_number)) { 
		array_push($errors, "task_id is required"); 
	}

	// register user if there are no errors in the form
	if (count($errors) == 0) {
			$query = "UPDATE analysis SET attendance=0 WHERE primary_key=$trainee_id'";
			mysqli_query($db, $query);
			
		}
	}
if (isset($_POST['password_btn'])) {
	change_password();
}

// Delete Question
function change_password(){
	// call these variables with the global keyword to make them available in function
	global $db, $errors, $username, $email;

	// receive all input values from the form. Call the e() function
    // defined below to escape form values
	$current_password   =  e($_POST['current_psw']);
	$new_password       =  e($_POST['new_psw']);
	$confirm_password   =  e($_POST['confirm_psw']);
	// form validation: ensure that the form is correctly filled
	if (empty($current_password)) { 
		array_push($errors, "current password is required"); 
	}
	if (empty($new_password)) { 
		array_push($errors, "New Password is required"); 
	}
	if (empty($confirm_password)){
		array_push($errors, "confirm_password is required");
	}

	// register user if there are no errors in the form
	if (count($errors) == 0) {
		if($new_password==$confirm_password){
			$query = "UPDATE empoly_details SET password=$new_password WHERE primary_key='"
    .$_SESSION['primary_key'].
    "'";
			mysqli_query($db, $query);
			}	
		}
	}